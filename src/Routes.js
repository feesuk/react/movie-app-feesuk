import Home from 'Screens/Home'
import WatchList from 'Screens/WatchList'
import About from 'Screens/About'
import Detail from 'Screens/Detail'
import Login from 'Screens/Login'
import Logout from 'Screens/Logout'

export const publicRoutes = [
	{
		component: Home,
		path: '/',
		exact: true,
	},
	{
		component: Login,
		path: '/login',
		exact: true,
	},
	{
		component: Logout,
		path: '/logout',
		exact: true,
	},
	{
		component: About,
		path: '/about',
		exact: true,
	},
	{
		component: Detail,
		path: '/movie/:id',
	},
	{
		component: WatchList,
		path: '/watch-list',
		exact: true,
	},
]
