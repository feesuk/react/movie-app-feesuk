import { combineReducers } from 'redux'

import movieDB from './movieDB'
import auth from './auth'

export default combineReducers({ movieDB, auth })
