import Cookies from 'js-cookie'

const initalState = {
	request_token: Cookies.get('request_token') || undefined,
	session_id: Cookies.get('session_id') || undefined,
	isAuthenticated: Cookies.get('session_id') && Cookies.get('request_token') ? true : false,
}

export default (state = initalState, action) => {
	switch (action.type) {
		case 'SET_REQUEST_TOKEN':
			return {
				...state,
				request_token: action.payload,
			}
		case 'SET_SESSION_ID':
			return {
				...state,
				session_id: action.payload,
				isAuthenticated: true,
			}
		case 'LOGOUT':
			return {
				request_token: undefined,
				session_id: undefined,
				isAuthenticated: false,
			}
		default:
			break
	}
	return state
}
