import API from 'API'
import Cookies from 'js-cookie'

export const requestToken = () => (dispatch) => {
	API.get(`/authentication/token/new?api_key=${process.env.REACT_APP_API_MOVIEDB_KEY}`).then((response) => {
		Cookies.set('request_token', response.data.request_token)
		dispatch({ type: 'SET_REQUEST_TOKEN', payload: response.data.request_token })
	})
}

export const login = (params) => async (dispatch) => {
	API.post(`/authentication/token/validate_with_login?api_key=${process.env.REACT_APP_API_MOVIEDB_KEY}`, { ...params, request_token: Cookies.get('request_token') })
		.then((response) => {
			console.log(response)
			if (response.status === 200) {
				API.post(`/authentication/session/new?api_key=${process.env.REACT_APP_API_MOVIEDB_KEY}`, { request_token: Cookies.get('request_token') }).then((response) => {
					if (response.status === 200) {
						Cookies.set('session_id', response.data.session_id)
						dispatch({
							type: 'SET_SESSION_ID',
							payload: response.data.session_id,
						})
					}
				})
			}
		})
		.catch((err) => console.log(err))
}

export const logout = () => async (dispatch) => {
	API.delete(`/authentication/session?api_key=${process.env.REACT_APP_API_MOVIEDB_KEY}`, { data: { session_id: Cookies.get('session_id') } }).then((response) => {
		if (response.status === 200) {
			Cookies.remove('session_id')
			Cookies.remove('request_token')
			Cookies.remove('session_id')
			dispatch({ type: 'LOGOUT' })
		}
	})
}
