import API from 'API'

export const getMovies = (params) => (dispatch) => {
	const { page = 1, genres = '' } = params
	API.get(`/discover/movie?api_key=${process.env.REACT_APP_API_MOVIEDB_KEY}&page=${page}&with_genres=${genres}`)
		.then((response) => {
			if (response.status === 200) {
				dispatch({
					type: 'GET_POKEMON',
					payload: response.data,
				})
			}
		})
		.catch((err) => console.log(err))
}

export const setFavoritMovie = (params) => (dispatch) => {
	dispatch({
		type: 'SET_FAVORIT_POKEMON',
		payload: params,
	})

	console.log(JSON.parse(localStorage.getItem('watchList')))

	!localStorage.getItem('watchList')
		? localStorage.setItem('watchList', JSON.stringify([{ ...params }]))
		: localStorage.setItem('watchList', JSON.stringify(JSON.parse(localStorage.getItem('watchList')).concat({ ...params })))
}

export const getGenres = () => (dispatch) => {
	API.get(`/genre/movie/list?api_key=${process.env.REACT_APP_API_MOVIEDB_KEY}`)
		.then((response) => {
			if (response.status === 200) {
				dispatch({
					type: 'GET_GENRE',
					payload: response.data.genres,
				})
			}
		})
		.catch((err) => console.log(err))
}
