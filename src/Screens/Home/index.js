import React, { Component, useCallback } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import InfiniteScroll from 'react-infinite-scroller'
import _ from 'lodash'

import Layout from 'Components/Layout'
import { getMovies, setFavoritMovie, getGenres } from 'Store/Action/MovieDB'

class Home extends Component {
	constructor() {
		super()
		this.state = {
			whichPage: 1,
			sortBy: 'release_date.desc',
			isLoading: true,
			isLoadingMore: false,
		}
	}

	componentDidMount() {
		this.props.getMovies({ page: this.state.whichPage })
		this.props.getGenres()
	}

	//contoh debounch untuk class component
	handleSearch = _.debounce((value) => alert(value), 1000)

	render() {
		const { whichPage } = this.state
		const { movies, genres, isAuthenticated } = this.props

		return (
			<Layout>
				<div className='home container'>
					<div className='home__title'>Movie List</div>

					<div className='home__filter'>
						<input type='text' placeholder='Search Movie' onChange={({ target: { value } }) => this.handleSearch(value)} />
					</div>
					<select
						className='selectCss--stretch '
						onChange={(event) => {
							this.setState({ whichPage: 1 })
							this.props.getMovies({ genres: event.target.value, page: this.state.whichPage })
						}}>
						<option value='' className='selectCss--emptyState'>
							Filter By Genre
						</option>
						{genres?.map((genre) => (
							<option value={genre.id} className=''>
								{genre.name}
							</option>
						))}
					</select>

					{!movies ? null : (
						<div className='home__grid '>
							{movies.results.map((movies, index) => {
								return (
									<div className='home__grid__item' key={index}>
										<button
											className='home__grid__item__save'
											onClick={() =>
												this.props.setFavoritMovie({
													id: movies.id,
													title: movies.title,
													poster_path: movies.poster_path,
												})
											}>
											+
										</button>

										<Link className='home__grid__item__content' to={`/movie/${movies.id}`}>
											{/* <img src={`${process.env.REACT_APP_API_IMAGE_URL}/${movies.poster_path}`} alt='' /> */}
											<div>{movies.title}</div>
										</Link>
									</div>
								)
							})}
						</div>
					)}
				</div>
			</Layout>
		)
	}
}

//map state to props adalag fungsi untuk me-mapping data dari store-redux ke komponen yang menggunakan (terbaca sebagai props)
const mapStateToProps = (state) => {
	return {
		favorits: state.movieDB.favoritMovies,
		movies: state.movieDB.movies,
		genres: state.movieDB.genres,
		isAuthenticated: state.auth.isAuthenticated,
	}
}

//export default connect(mapping-state-to-props, mapping-dispatch)(component-name)
export default connect(mapStateToProps, { getMovies, setFavoritMovie, getGenres })(Home)
