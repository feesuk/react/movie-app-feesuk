import { useEffect } from 'react'
import { connect } from 'react-redux'

import { logout } from 'Store/Action/Auth'

function Logout(props) {
	useEffect(() => {
		props.logout().then(() => props.history.push('/'))
	}, [])

	return <div>Loading..</div>
}

export default connect(null, { logout })(Logout)
