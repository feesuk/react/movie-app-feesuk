import React, { useEffect, useState } from 'react'
import { Switch, Route, NavLink } from 'react-router-dom'

import API from 'API'
import Layout from 'Components/Layout'
import DetailTabCharacter from './DetailTab/DetailTabCharacter'
import DetailTabOverview from './DetailTab/DetailTabOverview'
import DetailTabReview from './DetailTab/DetailTabReview'

export default function Detail(props) {
	const [movie, setMovie] = useState(null)

	const fetchDetailMovie = () => {
		API.get(`/movie/${props.match.params.id}?api_key=${process.env.REACT_APP_API_MOVIEDB_KEY}&language=en-US`)
			.then((response) => setMovie(response.data))
			.catch((err) => console.log(err))
	}
	useEffect(fetchDetailMovie, [])

	return (
		<Layout>
			<div className='detail'>
				<h1>Halaman Detail : {movie?.title}</h1>

				<div className='detail__banner'>
					<img src={`${process.env.REACT_APP_API_IMAGE_URL}/${movie?.backdrop_path}`} alt='' />
					<h1 className='detail__banner__desc'>ini text</h1>
				</div>

				<div>
					<NavLink className='detail__tab' activeClassName='detail__tab--active' exact to={`/movie/${props.match.params.id}`}>
						Review
					</NavLink>
					<NavLink className='detail__tab' activeClassName='detail__tab--active' exact to={`/movie/${props.match.params.id}/overview`}>
						Overview
					</NavLink>
					<NavLink className='detail__tab' activeClassName='detail__tab--active' exact to={`/movie/${props.match.params.id}/character`}>
						Character
					</NavLink>
				</div>

				<Switch>
					<Route path='/movie/:id/character' exact component={() => <DetailTabCharacter movie={movie} />} />
					<Route path='/movie/:id/overview' exact>
						<DetailTabOverview movie={movie} />
					</Route>
					<Route path='/movie/:id' exact component={DetailTabReview} />
				</Switch>
			</div>
		</Layout>
	)
}
