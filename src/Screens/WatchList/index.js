import React, { useCallback } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import _ from 'lodash'

import Layout from 'Components/Layout'

function WatchList(props) {
	//contoh debounch untuk functional component
	const handleSearch = useCallback(
		_.debounce((value) => alert(value), 1000),
		[]
	)

	return (
		<Layout>
			<div className='home'>
				<div className='home__title'>Poke Fav</div>
				<input type='text' placeholder='Search Movie' onChange={({ target: { value } }) => handleSearch(value)} />
				<div className='home__grid container'>
					{props.favorits.length === 0
						? null
						: props.favorits.map((movies, index) => {
								return (
									<div className='home__grid__item' key={index}>
										<Link className='home__grid__item__content' to={`/movie/${movies.id}`}>
											<img src={`${process.env.REACT_APP_API_IMAGE_URL}/${movies.poster_path}`} alt='' />
											<div>{movies.title}</div>
										</Link>
									</div>
								)
						  })}
				</div>
			</div>
		</Layout>
	)
}

const mapStateToProps = (state) => {
	return {
		favorits: state.movieDB.favoritMovies,
	}
}

export default connect(mapStateToProps)(WatchList)
