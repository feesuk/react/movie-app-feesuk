import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'

import Layout from 'Components/Layout'
import { requestToken, login } from 'Store/Action/Auth'

function Login(props) {
	//redux action
	const { requestToken, login } = props
	//redux state
	const { isAuthenticated } = props
	//local state
	const [loginData, setLoginData] = useState({ username: '', password: '' })

	useEffect(() => {
		if (isAuthenticated) props.history.push('/')
		requestToken()
		if (process.env.NODE_ENV === 'development') developmentMode()
	}, [])

	const handleChange = (event) => setLoginData({ ...login, [event.target.name]: event.target.value })

	const handleSubmit = (event) => {
		event.preventDefault()
		login(loginData).then(() => props.history.push('/'))
	}

	const developmentMode = () => {
		setLoginData({
			username: process.env?.REACT_APP_MOVIEDB_EMIAL,
			password: process.env?.REACT_APP_MOVIEDB_PASSWORD,
		})
	}

	return (
		<Layout>
			<div className='login'>
				<h1>Login </h1>
				<form onSubmit={handleSubmit} className='login__form'>
					<input value={loginData.username} type='text' name='username' placeholder='' onChange={handleChange} />
					<input value={loginData.password} type='password' name='password' placeholder='' onChange={handleChange} />
					<button type='submit'>Login</button>
				</form>
			</div>
		</Layout>
	)
}

const mapStateToProps = (state) => {
	return {
		isAuthenticated: state.auth.isAuthenticated,
	}
}

export default connect(mapStateToProps, { requestToken, login })(Login)
